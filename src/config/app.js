
const express = require("express");
const cors = require("cors");
const app = express();

app.use(express.json());
app.use(cors());
app.use('/images', express.static('src/images'));
const APPS = [
   'albums',
   'photos',
   'users'
]

for(const appName of APPS){
    const router = require(`../apps/${appName}/router.js`);
    app.use(router.getRouter())
}

module.exports = {
    app,
    APPS
};
