
const { BaseController } = require("flow-express/general/BaseController");
const { errorResponse } = require("flow-express/general/base.response");
const Photo = require("./models");
const Album = require("../albums/models")
const FileService = require("../../services/file.service")

class PhotosController extends BaseController {
    constructor() {
        super();
    }

    async post(req, res) {
        try {
            const { name,takenAt, description, image, albumId } = req.body;
            const imagePath= FileService.saveImage(image);
            const photo = await Photo.create({name,takenAt, description, image: imagePath, albumId });
            res.status(201).json(photo);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async get(req, res) {
        try {
            const photos = await Photo.findAll({where: req.query});
            res.status(200).json(photos);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async getOne(req, res) {
        try {
            const { id } = req.params;
            const photo = await Photo.findOne({ where: { id } });
            res.json(photo);
        } catch (error) {
            errorResponse(error, res);
        }
    }


    async put(req, res) {
        try {
            const { id } = req.params;
            const {name,takenAt, description, image, albumId } = req.body;
            const imagePath= FileService.saveImage(image);
            const photo = await Photo.update(
                {name,takenAt, description, image: imagePath, albumId  },
                {
                    where: {
                        id,
                    },
                },
            );
           
            
            res.json(photo);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async delete_(req, res) {
        try {
            const { id } = req.params;
            const photo = Photo.destroy({ where: { id } });
            res.status(204).json(photo);
        } catch (error) {
            errorResponse(error, res);
        }
    }
}

module.exports = PhotosController;
