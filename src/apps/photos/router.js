
const PhotosController = require("./controller");
const BaseRouter = require("flow-express/general/BaseRouter.js");

const controller = new PhotosController();
const router = new BaseRouter("/photos", ":id", controller);

module.exports = router;
