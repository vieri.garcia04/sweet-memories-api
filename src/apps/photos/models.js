
const { DataTypes } = require("sequelize");
const { IDField } = require("flow-express/db/base-fields");
const { sequelize } = require("../../config/db.config");
const AlbumModel = require("../albums/models");

const PhotosModel = sequelize.define("photos", {
	id: IDField,
    name: DataTypes.STRING(100),
    takenAt: DataTypes.DATE,
    description: DataTypes.STRING(500),
    image: DataTypes.STRING(500)
});

AlbumModel.hasMany(PhotosModel);
PhotosModel.belongsTo(AlbumModel);

module.exports =  PhotosModel;
