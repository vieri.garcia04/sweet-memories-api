
const { BaseController } = require("flow-express/general/BaseController");
const { errorResponse } = require("flow-express/general/base.response");
const Album = require("./models");
const Photo = require("../photos/models")
const { sequelize } = require("../../config/db.config");

class AlbumsController extends BaseController {
    constructor() {
        super();
    }

    async post(req, res) {
        try {
            const { name, userId } = req.body;
            const album = await Album.create({ name, userId });
            res.status(201).json(album);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async get(req, res) {
        try {
            const albums = await Album.findAll({
                attributes: {
                    include:[
                        [sequelize.fn('COUNT', sequelize.col('photos.id')), 'total_photos']
                    ]
                },
                include: [
                    {
                        model: Photo,
                        attributes: ['image']
                    }
                ],
                group: 'albums.id',
            })

            res.status(200).json(albums);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async getOne(req, res) {
        try {
            const { id } = req.params;
            const album = await Album.findOne({ where: { id } });
            res.json(album);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async put(req, res) {
        try {
            const { id } = req.params;
            const { name, userId } = req.body;
            const album = await Album.update(
                { name, userId },
                {
                    where: {
                        id,
                    },
                },
            );
            res.status(200).json(album);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async delete_(req, res) {
        try {
            const { id } = req.params;
            const album = Album.destroy({ where: { id } });
            res.status(204).json(album);
        } catch (error) {
            errorResponse(error, res);
        }
    }
}

module.exports = AlbumsController;
