
const { DataTypes } = require("sequelize");
const { IDField } = require("flow-express/db/base-fields");
const { sequelize } = require("../../config/db.config");

const User = require("../users/models")
const AlbumsModel = sequelize.define("albums", {
	id: IDField,
    name: DataTypes.STRING(100),
});
User.hasMany(AlbumsModel);
AlbumsModel.belongsTo(User);

module.exports =  AlbumsModel;
