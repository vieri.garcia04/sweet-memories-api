
const AlbumsController = require("./controller");
const BaseRouter = require("flow-express/general/BaseRouter.js");

const controller = new AlbumsController();
const router = new BaseRouter("/albums", ":id", controller);

module.exports = router;
