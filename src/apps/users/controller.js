const { BaseController } = require("flow-express/general/BaseController");
const { errorResponse } = require("flow-express/general/base.response");
const User = require("./models");
const Albums = require('../albums/models')
const Photos = require('../photos/models')
const { sequelize } = require("../../config/db.config");
class UsersController extends BaseController {
    constructor() {
        super();
    }

    async post(req, res) {
        try {
            const { names, lastname, birthday, email, password } = req.body;
            const user = await User.create({ names, lastname, birthday, email, password });
            res.status(201).json(user);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async get(req, res) {
        try {
            const user = await User.findAll();
            res.status(200).json(user);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async getOne(req, res) {
        try {
            const { id } = req.params;
            const user = await User.findOne({
                attributes: {
                    include: [
                        [sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('albums.id'))), 'total_albums']
                    ]
                },
                include: [
                    {
                        model: Albums,
                        include: [
                            {
                                model: Photos,
                                attributes: []
                            }
                        ],
                        attributes: [[sequelize.fn('COUNT', sequelize.fn('DISTINCT', sequelize.col('albums->photos.id'))), 'total_photos']],
                        group: 'albums.id'
                    }
                ],
                where: { id },


            });
            res.status(200).json(user);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async put(req, res) {
        try {
            const { id } = req.params;
            const { names, lastname, birthday, email, password } = req.body;
            const user = await User.update(
                { names, lastname, birthday, email, password },
                {
                    where: {
                        id,
                    },
                },
            );
            res.status(200).json(user);
        } catch (error) {
            errorResponse(error, res);
        }
    }

    async delete_(req, res) {
        try {
            const { id } = req.params;
            const user = User.destroy({ where: { id } });
            res.status(204).json(user);
        } catch (error) {
            errorResponse(error, res);
        }
    }
}

module.exports = UsersController;
