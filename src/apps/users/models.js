
const { DataTypes } = require("sequelize");
const { IDField } = require("flow-express/db/base-fields");
const { sequelize } = require("../../config/db.config");

const UsersModel = sequelize.define("users", {
	id: IDField,
    names: DataTypes.STRING(100),
    lastname: DataTypes.STRING(100),
    birthday: DataTypes.DATE,
    email: DataTypes.STRING(150),
    password: DataTypes.STRING(255)
});

module.exports =  UsersModel;
