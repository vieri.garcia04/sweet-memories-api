var fs = require('fs');
axios = require('axios')

class FileService {

    static readFile = (path) => {
        return fs.readFileSync(path, (e) => { console.log(e) });
    }

    static writeFile = (path, data) => {
        try {
            console.log(path, data)
            return fs.writeFileSync(path, data);
        } catch (error) {
            console.error("Can't writte to file:", error)
        }

    }

    static getFilename = (path) => {
        return path.replace(/^.*[\\\/]/, '')
    }

    static saveImage = (image) => {
        const fileName = this.getFilename(image)
        const imagePath = `/images/${fileName}`
        if (this.isInternetImage(image)) {
            this.downloadImage(`src${imagePath}`, image)
        } else {
            const imageData = this.readFile(image)
            this.writeFile(`src${imagePath}`, imageData)
        }
        return imagePath


    }

    static isInternetImage = (url) => {
        const regex = /(http[s]*:\/\/)([a-z\-_0-9\/.]+)\.([a-z.]{2,3})\/([a-z0-9\-_\/._~:?#\[\]@!$&'()*+,;=%]*)([a-z0-9]+\.)(jpg|jpeg|png)/i;
        let result= false;
        if (url.match(regex)) {
            result=true;
        }
        return result;
    }

    static downloadImage = (path, image_url) => {
        axios({
            url: image_url,
            responseType: 'stream'
        }
        ).then(
            response =>
                new Promise((resolve, reject) => {
                    response.data
                        .pipe(fs.createWriteStream(path))
                        .on('finish', () => resolve())
                        .on('error', e => reject(e));
                }),
        );
    }
}

module.exports = FileService