
const APP_PORT = process.env.PORT || 5005;

const { app, APPS } = require("./src/config/app");
const { syncTables, authenticate } = require("./src/config/db.config");


app.listen(APP_PORT, () => {
    try{
        const models = APPS.map(app => require(`./src/apps/${app}/models.js`));
        authenticate();
        syncTables(models);
        console.log("Listening on port " + APP_PORT)
    } catch(error){
        console.error(error)
    }
});
